<?php use yii\widgets\ActiveForm; ?>

<h1>Reports</h1>

<h3>Data upload</h3>

<?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]) ?>

<?= $form->field($model, 'file')->fileInput() ?>

<button>Submit</button>

<?php ActiveForm::end() ?>


<?php if($graph): ?>

<div id="chart-container" style="width:100%; height:400px;"></div>

<script>
    document.addEventListener('DOMContentLoaded', function () {
        Highcharts.chart('chart-container', {
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie'
            },
            title: {
                text: '<?php echo $graph['title']; ?>'
            },
            tooltip: {
                pointFormat: '{series.name}: <strong>{point.percentage:.1f}%</strong>'
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: true,
                        format: '<strong>{point.name}</strong>: {point.percentage:.1f} %'
                    }
                }
            },
            series: [{
                name:  '<?php echo $graph['valueLabel']; ?>',
                colorByPoint: true,
                data: <?php echo json_encode($graph['data']); ?>
            }]
        });
    });
</script>
<?php endif; ?>