<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\ReportForm;
use yii\web\UploadedFile;

class SiteController extends Controller
{

    private $saveDir = 'uploads';

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'reports'],
                'rules' => [
                    [
                        'actions' => ['logout', 'reports'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }


    public function actionReports() {

        $retData = [];
        $model = new ReportForm();
        
        if(Yii::$app->request->isPost) {
            $model->file = UploadedFile::getInstance($model, 'file');

            if($model->file && $model->validate()) {
                $graph = [];

                $model->file->saveAs($this->saveDir . '/' . $model->file->baseName . '.' . $model->file->extension);

                $spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($this->saveDir . '/' . $model->file->baseName . '.' . $model->file->extension);

                // TODO: Get a count of the sheets in the spread sheet and add validation accordingly.
                $spreadsheet->setActiveSheetIndex(0);
                $graph['title'] = array(0, $spreadsheet->getActiveSheet()->toArray(null, true, true, true))[1][1]['A'];

                $spreadsheet->setActiveSheetIndex(1);
                $spreadsheetData = array($spreadsheet->getActiveSheet()->toArray(null, true, true, true))[0];

                $graph['valueLabel'] = $spreadsheetData[1]['A'];
                $graph['unitLabel'] = $spreadsheetData[1]['B'];

                $graph['data'] = [];

                $totalCount = array_sum(array_column($spreadsheetData, 'A'));

                for($i = 2; $i<=count($spreadsheetData); $i++) {
                    if(null != $spreadsheetData[$i]['A']) {
                        $graph['data'][] = [
                            'name' => [$spreadsheetData[$i]['B']],
                            'y' => ( $spreadsheetData[$i]['A'] / $totalCount ) * 100,
                            'sliced' => true,
                            'selected' => false,
                            ];
                    }
                }
            }
        }

        return $this->render('reports', [
                    'model' => $model,
                    'graph' => $graph
                ]);
    }


    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
}
